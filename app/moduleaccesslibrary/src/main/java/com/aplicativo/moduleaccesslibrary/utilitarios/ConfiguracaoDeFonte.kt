package com.aplicativo.moduleaccesslibrary.utilitarios

import com.aplicativo.moduleaccesslibrary.enumbase.EspacamentoLetras
import com.aplicativo.moduleaccesslibrary.enumbase.EspacamentoLinhas
import com.aplicativo.moduleaccesslibrary.enumbase.RecuoEsquerda
import com.aplicativo.moduleaccesslibrary.enumbase.TamanhoFonte
import com.pax.dal.entity.EFontTypeAscii

object ConfiguracaoDeFonte {

    fun obterFonte(tamanhoFonte: TamanhoFonte): EFontTypeAscii {

        return when (tamanhoFonte) {
            TamanhoFonte.PEQUENA -> EFontTypeAscii.FONT_8_16
            TamanhoFonte.NORMAL -> EFontTypeAscii.FONT_8_32
            TamanhoFonte.MEDIA -> EFontTypeAscii.FONT_12_24
            TamanhoFonte.GRANDE -> EFontTypeAscii.FONT_12_48
            TamanhoFonte.EXTRA_GRANDE -> EFontTypeAscii.FONT_32_48
        }

    }

    fun obterEspacamentoLetras(espacamentoLetras: EspacamentoLetras): Int {
        return when (espacamentoLetras) {
            EspacamentoLetras.NORMAL -> 0
            EspacamentoLetras.GRANDE -> 16
            EspacamentoLetras.EXTRA_GRANDE -> 32
        }
    }

    fun obterEspacamentoLinhas(espacamentoLinhas: EspacamentoLinhas): Int {
        return when (espacamentoLinhas) {
            EspacamentoLinhas.NORMAL -> 0
            EspacamentoLinhas.GRANDE -> 16
            EspacamentoLinhas.EXTRA_GRANDE -> 32
        }
    }

    fun obterRecuoEsquerda(recuoEsquerda: RecuoEsquerda): Int {
        return when (recuoEsquerda) {
            RecuoEsquerda.TAB_ZERO -> 0
            RecuoEsquerda.TAB_UM -> 8
            RecuoEsquerda.TAB_DOIS -> 16
            RecuoEsquerda.TAB_TRES -> 32
            RecuoEsquerda.TAB_QUATRO -> 64
        }
    }

}


