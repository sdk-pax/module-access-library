package com.aplicativo.moduleaccesslibrary.utilitarios

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.aplicativo.moduleaccesslibrary.R
import com.aplicativo.moduleaccesslibrary.modelo.Comprovante
import kotlinx.android.synthetic.main.recibo.view.*

object ConversorLayout {


    fun converterResourceEmBitmap(contexto: Context, resource: Int): Bitmap {
        val options = BitmapFactory.Options()

        return BitmapFactory.decodeResource(contexto.resources, resource, options)
    }

    fun criarBitmapDaView(contexto: Context, comprovante: Comprovante?, logo: Int): Bitmap? {
        val mInflate = LayoutInflater.from(contexto).inflate(R.layout.recibo, null)
        mInflate.ivLogoRecibo.setImageResource(logo)
        comprovante?.let {
            carregaViewComDados(contexto, mInflate, comprovante)
        }
        val constraintLayout = mInflate.constraintLayout as ConstraintLayout

        return criarBitmapDoLayout(constraintLayout)
    }

    private fun criarBitmapDoLayout(constraintLayout: ConstraintLayout): Bitmap? {
        constraintLayout.measure(
            View.MeasureSpec.makeMeasureSpec(convertDpToPixels(328), View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        constraintLayout.layout(
            0,
            0,
            constraintLayout.measuredWidth,
            constraintLayout.measuredHeight
        )

        val bitmap = Bitmap.createBitmap(
            constraintLayout.measuredWidth,
            constraintLayout.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        constraintLayout.draw(canvas)
        return bitmap
    }


    private fun carregaViewComDados(contexto: Context, mInflate: View, comprovante: Comprovante?) {

        comprovante?.apply {
            bandeira?.let { mInflate.tvBandeira?.text = bandeira }
            if (numeroCartao != null) {
                mInflate.tvNumeroCartao?.text = contexto.getString(
                    R.string.label_numero_cartao,
                    numeroCartao.substring(numeroCartao.length - 4)
                )
            }
            numeroPOS?.let {
                mInflate.tvNumeroPos?.text =
                    contexto.getString(R.string.label_numero_POS, numeroPOS)
            }
            cnpjDoEstabelecimento?.let {
                mInflate.tvCnpj?.text =
                    contexto.getString(R.string.label_cnpj, cnpjDoEstabelecimento)
            }
            nomeDaLoja?.let { mInflate.tvNomeDaLoja?.text = nomeDaLoja }
            cidadeUF?.let {
                mInflate.tvCidadeUf?.text = cidadeUF
            }
            numeroEstabelecimento?.let {
                mInflate.tvNumeroEstabelecimento?.text = numeroEstabelecimento
            }
            numeroDocumento?.let {
                mInflate.tvNumeroDocumento?.text =
                    contexto.getString(R.string.label_numero_documento, numeroDocumento)
            }
            numeroDaAutorizacao?.let {
                mInflate.tvNumeroAutorizacao?.text =
                    contexto.getString(R.string.label_numero_autorizacao, numeroDaAutorizacao)
            }
            dataPagamento?.let {
                mInflate.tvData?.text = dataPagamento
            }
            hora?.let {
                mInflate.tvHora?.text = hora
            }

            enderecoDaLoja?.let {
                mInflate.tvEnderecoLoja?.text = enderecoDaLoja
            }

            tipoDePagamento?.let {
                mInflate.tvTipoPagamento?.text = tipoDePagamento
            }

            valor?.let {
                mInflate.tvValor?.text = valor
            }

        }
    }

    fun convertDpToPixels(dp: Int): Int {
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                Resources.getSystem().displayMetrics
            )
        )
    }


}