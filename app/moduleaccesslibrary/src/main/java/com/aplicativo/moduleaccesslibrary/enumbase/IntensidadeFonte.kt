package com.aplicativo.moduleaccesslibrary.enumbase

enum class IntensidadeFonte(val nivel : Int) {

    NORMAL (1),
    MEDIO (3),
    INTENSO (4)

}