package com.aplicativo.moduleaccesslibrary.modulos


import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.aplicativo.moduleaccesslibrary.R
import com.aplicativo.moduleaccesslibrary.base.BaseDeImpressora
import com.aplicativo.moduleaccesslibrary.enumbase.*
import com.aplicativo.moduleaccesslibrary.modelo.Comprovante
import com.aplicativo.moduleaccesslibrary.utilitarios.ConfiguracaoDeFonte
import com.aplicativo.moduleaccesslibrary.utilitarios.ConversorLayout
import com.pax.dal.entity.EFontTypeExtCode

class ModuloDeImpressao(private val context: Context) {

    init {
        BaseDeImpressora.setupPrinter(context)
    }

    fun imprimirTexto(
        texto: String?,
        tamanho: TamanhoFonte = TamanhoFonte.NORMAL,
        espacamentoLetras: EspacamentoLetras = EspacamentoLetras.NORMAL,
        espacamentoLinhas: EspacamentoLinhas = EspacamentoLinhas.NORMAL,
        recuoEsquerda: RecuoEsquerda = RecuoEsquerda.TAB_UM,
        intensidade: IntensidadeFonte = IntensidadeFonte.NORMAL
    ) {

        texto?.let {
            Thread(Runnable {
                BaseDeImpressora.init()
                BaseDeImpressora.setSizeFont(
                    ConfiguracaoDeFonte.obterFonte(tamanho),
                    EFontTypeExtCode.FONT_24_24
                )
                BaseDeImpressora.setSpaceFont(
                    ConfiguracaoDeFonte.obterEspacamentoLetras(espacamentoLetras).toByte(),
                    ConfiguracaoDeFonte.obterEspacamentoLinhas(espacamentoLinhas).toByte()
                )
                BaseDeImpressora.setLeftIntent(ConfiguracaoDeFonte.obterRecuoEsquerda(recuoEsquerda))
                BaseDeImpressora.setIntentFont(intensidade.nivel)
                BaseDeImpressora.printStr(texto, null)
                BaseDeImpressora.start()
            }).start()
        } ?: kotlin.run {
            Log.e("TEXTO", "ESTÁ VAZIO")
        }
    }

    fun imprimirImagemPadrao(logo: Int = R.drawable.pax_icon) {
        val imagemBitmap = ConversorLayout.converterResourceEmBitmap(context, logo)
        Thread(Runnable {
            BaseDeImpressora.init()
            BaseDeImpressora.printImage(imagemBitmap)
            BaseDeImpressora.start()
        }).start()
    }


    fun imprimirReciboPadrao(
        comprovante: Comprovante? = null,
        logo: Int = R.drawable.itbam_black
    ) {
        val reciboBitmap = ConversorLayout.criarBitmapDaView(context, comprovante, logo)
        reciboBitmap?.let {
            executarImpressaoDeRecibo(reciboBitmap)
        }

    }

    private fun executarImpressaoDeRecibo(reciboBitmap: Bitmap) {
        Thread(Runnable {
            BaseDeImpressora.init()
            BaseDeImpressora.printBitmapWithMonoThreshold(reciboBitmap, 250)
            BaseDeImpressora.step(50)
            BaseDeImpressora.start()
        }).start()
    }



}
