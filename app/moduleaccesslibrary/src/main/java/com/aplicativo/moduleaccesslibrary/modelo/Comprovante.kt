package com.aplicativo.moduleaccesslibrary.modelo

class Comprovante(
    val tipoDePagamento: String? = null,
    val numeroCartao: String?= null,
    val numeroPOS: String?= null,
    val cnpjDoEstabelecimento: String?= null,
    val nomeDaLoja: String?= null,
    val enderecoDaLoja: String?= null,
    val numeroEstabelecimento: String?= null,
    val numeroDocumento: String?= null,
    val numeroDaAutorizacao: String?= null,
    val dataPagamento: String?= null,
    val valor: String?= null,
    val bandeira: String?= null,
    val cidadeUF: String?= null,
    val hora: String?= null
)