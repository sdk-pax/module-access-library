package com.aplicativo.moduleaccesslibrary.enumbase

enum class TamanhoFonte {


    PEQUENA,
    MEDIA,
    NORMAL,
    GRANDE,
    EXTRA_GRANDE

}