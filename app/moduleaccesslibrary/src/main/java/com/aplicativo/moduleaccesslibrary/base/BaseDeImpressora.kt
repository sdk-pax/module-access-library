package com.aplicativo.moduleaccesslibrary.base


import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.pax.dal.IDAL
import com.pax.dal.IPrinter
import com.pax.dal.entity.EFontTypeAscii
import com.pax.dal.entity.EFontTypeExtCode
import com.pax.dal.exceptions.PrinterDevException
import com.pax.neptunelite.api.NeptuneLiteUser

object BaseDeImpressora {

    private val TAG = "IMPRESSORA-BASE"

    private var dal: IDAL? = null
    private var neptuneLiteUser: NeptuneLiteUser? = null
    private var iPrinter: IPrinter? = null

    fun setupPrinter(context: Context) {
        neptuneLiteUser = NeptuneLiteUser.getInstance()
        neptuneLiteUser?.let {
            dal = neptuneLiteUser!!.getDal(context)
        }
        dal?.let {
            iPrinter = dal!!.printer
        }

    }

    fun printBitmapWithMonoThreshold(bitmap: Bitmap, grayThreshold: Int) {
        try {
            iPrinter?.printBitmapWithMonoThreshold(bitmap, grayThreshold)
        } catch (e: PrinterDevException) {
            Log.e(TAG, "NÃO FOI POSSÍVEL REALIZAR IMPRESSÃO")
        }
    }


    fun step(pixel: Int) {
        try {
            iPrinter?.step(pixel)
        } catch (e: PrinterDevException) {
            Log.e(TAG, "PROBLEMAS COM STEP")
        }
    }

    fun init() {
        try {
            iPrinter?.init()
        } catch (ex: PrinterDevException) {
            Log.e(TAG, "NÃO FOI POSSÍVEL INICIAR IMPRESSÃO")
        }
    }

    fun start() {
        try {
            iPrinter?.start()
        } catch (ex: PrinterDevException) {
            Log.e(TAG, "NÃO FOI POSSÍVEL STARTAR IMPRESSÃO")
        }
    }

    fun printStr(string1: String, string2: String?) {
        try {
            iPrinter?.printStr(string1, string2)
        } catch (ex: PrinterDevException) {
            Log.e(TAG, "NÃO FOI POSSÍVEL REALIZAR IMPRESSÃO")
        }
    }

    fun printImage(image: Bitmap) {
        try {
            iPrinter?.printBitmap(image)
        } catch (e: PrinterDevException) {
            Log.e(TAG, "NÃO FOI POSSÍVEL REALIZAR IMPRESSÃO")
        }
    }

    fun setSizeFont(asciiFontType: EFontTypeAscii, cFontType: EFontTypeExtCode){

        try {
            iPrinter!!.fontSet(asciiFontType,cFontType )
        }catch (e : PrinterDevException){
            e.printStackTrace()
            Log.e("SETFONT", e.printStackTrace().toString())
        }
    }

     fun setSpaceFont(wordSpace: Byte, lineSpace: Byte){
        try {
            iPrinter!!.spaceSet(wordSpace, lineSpace)
        }catch (e: PrinterDevException){
            e.printStackTrace()
            Log.e("SETFONT", e.printStackTrace().toString())
        }
    }

     fun setLeftIntent(intent : Int){
        try {
            iPrinter!!.leftIndent(intent)
        }catch (e: PrinterDevException){
            e.printStackTrace()
            Log.e("SETFONT", e.printStackTrace().toString())
        }
    }

    fun setIntentFont(intent: Int){
        try {
            iPrinter!!.setGray(intent)
        }catch (e: PrinterDevException){
            e.printStackTrace()
            Log.e("SETFONT", e.printStackTrace().toString())
        }

    }


}
