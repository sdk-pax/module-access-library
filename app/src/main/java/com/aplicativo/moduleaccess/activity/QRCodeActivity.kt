package com.aplicativo.moduleaccess.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.aplicativo.moduleaccess.R
import com.aplicativo.moduleaccesslibrary.modulos.ModuloDeQrCode

import kotlinx.android.synthetic.main.activity_q_r_code.*

class QRCodeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_q_r_code)

        btn_ler_qrcode_activity.setOnClickListener {
            ModuloDeQrCode().iniciarScanner(this)
        }

        btn_gerar_qrcode_activity.setOnClickListener {
            img_qrcode_activity.setImageBitmap(ModuloDeQrCode().gerarQrCode(txt_gera_qrcode_activity.text.toString().trim()))
        }

        btn_camera_activity.setOnClickListener {
            Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE).also {
                startActivityForResult(it,1)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        txt_qrcode_activity.text =  ModuloDeQrCode().lerQrCode(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)
    }
}
