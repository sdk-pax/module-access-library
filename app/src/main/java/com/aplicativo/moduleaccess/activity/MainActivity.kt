package com.aplicativo.moduleaccess.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aplicativo.moduleaccess.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}
