package com.aplicativo.moduleaccess.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aplicativo.moduleaccesslibrary.modelo.Comprovante
import androidx.fragment.app.Fragment
import com.aplicativo.moduleaccess.R
import com.aplicativo.moduleaccesslibrary.modulos.ModuloDeImpressao
import kotlinx.android.synthetic.main.fragment_impressora.*


/**
 * A simple [Fragment] subclass.
 * Use the [ImpressoraFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ImpressoraFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_impressora, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnImprimir.setOnClickListener {
            ModuloDeImpressao(requireContext()).imprimirTexto("IMPRIMINDO TEXTO......")
        }

        btnImprimirImagemPadrao.setOnClickListener {
            ModuloDeImpressao(requireContext()).imprimirImagemPadrao()
        }

        btnImprimirReciboPadrao.setOnClickListener {
            ModuloDeImpressao(requireContext()).imprimirReciboPadrao()
        }

        btnImprimirReciboPadraoComValores.setOnClickListener {
            val comprovante = Comprovante(
                "Crédito",
                "2123131.454545.45455",
            "123478",
                "00.444.555/4565-00",
                "INSTITUTO TRANSIRE",
                "Parque Industrial - Rua Anhandui, 520",
                "1234567891234567",
            "123456",
                "654321",
                "02/06/2020",
                "520,00",
                "MASTERCARD",
                "MANAUS - AM",
                "16:36"
            )
            ModuloDeImpressao(requireContext()).imprimirReciboPadrao(comprovante, R.drawable.itbam_black)
        }
    }

}
