package com.aplicativo.moduleaccess.ui.fragment

import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.aplicativo.moduleaccess.R
import com.aplicativo.moduleaccesslibrary.modulos.ModuloDeImpressao
import com.aplicativo.moduleaccesslibrary.modulos.ModuloDeQrCode
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_leitor_qr_code.*


/**
 * A simple [Fragment] subclass.
 */
class LeitorQrCodeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_leitor_qr_code, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_ler_qrcode_fragment.setOnClickListener {
            ModuloDeQrCode().iniciarScanner(this)
        }

        btn_camera_fragment.setOnClickListener {
            Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE).also {
                startActivityForResult(it,1)
            }
        }

        btn_gerar_qrcode_fragment.setOnClickListener {
            img_qrcode_fragment.setImageBitmap(ModuloDeQrCode().gerarQrCode(txt_gera_qrcode_fragment.text.toString().trim()))
        }

//        img_qrcode_fragment.setOnClickListener {
//
//         //   img_qrcode_fragment.buildDrawingCache()
//         //   val bitmap = img_qrcode_fragment.getDrawingCache() as Bitmap
//
//            val image = (img_qrcode_fragment.drawable as BitmapDrawable).bitmap

//            ModuloDeImpressao(requireContext()).imprimirBitmap(image)
//        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        txt_qrcode_fragment.text =
            ModuloDeQrCode().lerQrCode(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)

    }


}
//class test(val nome: String, val cpf: String)

