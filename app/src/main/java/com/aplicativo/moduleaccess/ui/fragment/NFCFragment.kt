package com.aplicativo.moduleaccess.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aplicativo.moduleaccess.R

import kotlinx.android.synthetic.main.fragment_nfc.*

/**
 * A simple [Fragment] subclass.
 * Use the [NFCFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NFCFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_nfc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}
