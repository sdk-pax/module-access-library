package com.aplicativo.moduleaccess.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.aplicativo.moduleaccess.R
import kotlinx.android.synthetic.main.fragment_leitor_tarja_magnetica.*

/**
 * A simple [Fragment] subclass.
 */
class LeitorTarjaMagneticaFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leitor_tarja_magnetica, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)




    }

}
