package com.aplicativo.moduleaccess.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController

import com.aplicativo.moduleaccess.R
import com.aplicativo.moduleaccess.activity.QRCodeActivity
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_go_printer.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_impressoraFragment)
        }

        btn_go_chip.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_leitorChipFragment)
        }

        btn_go_magnetic.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_leitorTarjaMagneticaFragment)
        }

        btn_go_qrcode_activity.setOnClickListener {
            val intent = Intent(requireContext(),QRCodeActivity::class.java)
            startActivity(intent)
        }

        btn_go_qrcode_fragment.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_leitorQrCodeFragment)
        }

        btn_go_nfc.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_NFCFragment)
        }

    }

}
